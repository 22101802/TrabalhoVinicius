//
//  ViewController.swift
//  TrabalhoVinicius
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct personagem: Decodable{
    let image: String
    let name: String
    let actor: String
    
}

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaPersonagem.count
    }
    

    @IBOutlet var tableView: UITableView!
    var listaPersonagem: [personagem]=[]
    var lista:[personagem] = []
    var useDefaults = UserDefaults.standard
    var listaKey="lista"
    
    func tableView( _ tableView:UITableView,cellForRowAt indexPath:IndexPath)-> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let list = listaPersonagem[indexPath.row]
        
        cell.atorNome.text = list.name
        cell.personagemNome.text = list.actor
        cell.imagem?.kf.setImage(with: URL(string: list.image))
        
        return cell
    }
    
    func personagemNovo(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of : [personagem].self){ response in if
            let personagem = response.value{
            self.listaPersonagem = personagem
        }
        self.tableView.reloadData()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        personagemNovo()
    }


}

